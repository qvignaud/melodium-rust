[package]
name = "melodium"
version = "0.8.0"
authors = ["Quentin VIGNAUD <quentin.vignaud@melodium.tech>"]
rust-version = "1.83"
edition = "2018"
license = "EUPL-1.2"
homepage = "https://melodium.tech"
repository = "https://gitlab.com/melodium/melodium"
readme = "README.md"
description = "Dataflow-oriented language & tool, focusing on treatments applied on data, allowing high scalability and massive parallelization safely"
keywords = ["dataflow","parallelization","scalability","async","signal"]
categories = ["compilers", "science", "concurrency", "asynchronous"]

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[lib]
name = "melodium"

[[bin]]
name = "melodium"
required-features = ["cli"]

[dependencies]
async-std = "1.12"
clap = { version = "4.0.32", features = ["cargo", "derive", "string"], optional = true }
colored = { version = "2.0.2", optional = true }
melodium-common = { path = "../melodium-common", version = "0.8.0" }
melodium-core = { path = "../melodium-core", version = "0.8.0", optional = true  }
melodium-engine = { path = "../melodium-engine", version = "0.8.0" }
melodium-lang = { path = "../melodium-lang", version = "0.8.0" }
melodium-loader = { path = "../melodium-loader", version = "0.8.0" }
melodium-distribution = { path = "../melodium-distribution", version = "0.8.0", optional = true  }
melodium-doc = { path = "../melodium-doc", version = "0.8.0", optional = true }
uuid = { version = "1.5.0", features = ["serde"], optional = true }

std-mel = { path = "../libs/std-mel", version = "0.8.0" }

distrib-mel = { path = "../libs/distrib-mel", version = "0.8.0", optional = true }
encoding-mel = { path = "../libs/encoding-mel", version = "0.8.0", optional = true }
fs-mel = { path = "../libs/fs-mel", version = "0.8.0", optional = true }
http-mel = { path = "../libs/http-mel", version = "0.8.0", optional = true }
javascript-mel = { path = "../libs/javascript-mel", version = "0.8.0", optional = true }
json-mel = { path = "../libs/json-mel", version = "0.8.0", optional = true }
net-mel = { path = "../libs/net-mel", version = "0.8.0", optional = true }
process-mel = { path = "../libs/process-mel", version = "0.8.0", optional = true }
regex-mel = { path = "../libs/regex-mel", version = "0.8.0", optional = true }
sql-mel = { path = "../libs/sql-mel", version = "0.8.0", optional = true }
work-mel = { path = "../libs/work-mel", version = "0.8.0", optional = true }

[features]
default = ["cli", "standard-edition"]
cli = ["clap", "colored"]
jeu = ["melodium-loader/jeu"]
distribution = ["melodium-distribution", "distrib-mel", "work-mel", "uuid"]
doc = ["melodium-doc", "melodium-engine/doc", "melodium-core/doc"]
network = ["net-mel", "http-mel", "melodium-loader/network"]
fs = ["fs-mel", "process-mel", "melodium-loader/filesystem"]
text = ["encoding-mel", "regex-mel"]
javascript = ["javascript-mel", "json-mel"]
sql = ["sql-mel"]
standard-edition = ["jeu", "doc", "distribution", "fs", "network", "text", "javascript", "sql"]
container-edition = ["standard-edition", "work-mel/kubernetes"]

[target.wasm32-unknown-unknown.dependencies]
getrandom = { version = "0.2", features = ["js"] }

[package.metadata.docs.rs]
all-features = true
rustdoc-args = ["--cfg", "docsrs"]

[package.metadata.deb]
section = "devel"
priority = "optional"
